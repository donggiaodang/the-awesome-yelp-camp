
const path = require('path')
const mongoose = require('mongoose')
const cities = require('./cities')
const {places, descriptors} = require('./seedHelpers')
const Campground = require('../models/campground');

mongoose.connect('mongodb://localhost:27017/yelp-camp',{
    useNewUrlParser: true,
   
    useUnifiedTopology:true
})

const db = mongoose.connection;
db.on('error',console.error.bind(console,'connection error:'));
db.once('open',() => {
    console.log("Database connected")
});
const sample = (array) => array[Math.floor(Math.random()* array.length)];
const seedDB = async () =>{
    await Campground.deleteMany({});
    for(let i = 0; i<200; i++){
        const random1000 = Math.floor(Math.random()*1000);
        const price = Math.floor(Math.random() *20) +10;
        const camp = new Campground({
            author: '63556ee5939433334d37c9b0',
            location: `${cities[random1000].city} , ${cities[random1000].state}`,
            title: `${sample(descriptors)} ${sample(places)}`,
            images:  [
                {
                  url: 'https://res.cloudinary.com/dd84mw590/image/upload/v1670159245/YelpCamp/rvyqyo6lxkhvabz4yeei.jpg',
                  filename: 'YelpCamp/rvyqyo6lxkhvabz4yeei',
                },
                {
                  url: 'https://res.cloudinary.com/dd84mw590/image/upload/v1671385056/YelpCamp/kpzzhjvusqrnlbadioia.jpgno',
                  filename: 'YelpCamp/kpzzhjvusqrnlbadioia',
                },
                {
                  url: 'https://res.cloudinary.com/dd84mw590/image/upload/v1670159251/YelpCamp/ybghvdee3h6wgl9hpquj.jpg',
                  filename: 'YelpCamp/ybghvdee3h6wgl9hpquj',
                }
              ],
 
            description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
            price,
            geometry: {
              type: 'Point',
              coordinates: [cities[random1000].longitude,
                            cities[random1000].latitude
            ]
            }
        })
        await camp.save();
    }
}

seedDB().then (()=>{
mongoose.connection.close();
})